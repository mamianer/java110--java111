package tests;

import common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class EditPage extends Utils {
    public static String url = "http://192.168.47.135:8653/blog_system/blog_edit.html";
    public EditPage() {
        super(url);
    }

    public void EditSuc() throws InterruptedException {

        String blogTile = "Java110&111 Autest01";
        driver.findElement(By.cssSelector("#title")).sendKeys(blogTile);

//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
//        wait.until(ExpectedConditions.presenceOfElementLocated());

        //无法输入博客内容---怎么办？
        //两个解决办法：
        //1）博客内容本身就有默认内容，我们不需要手动实现
        //2）还未学到---通过鼠标操作来实现
//        WebElement ele = driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre"));
//        ele.sendKeys("比特就业课Java110&111班级");//出现错误

        //第二种方法实现----下节课再讲
//        1.鼠标先挪动到博客内容区域
//        2.双击鼠标将内容删掉：鼠标双击内容+键盘DELETE
//        3.输入内容
//        WebElement ele = driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre"));
//        Actions actions = new Actions(driver);
//        //perform作用：为了在页面看到效果
//        Thread.sleep(3000);
//        actions.doubleClick(ele).perform();
//        Thread.sleep(3000);
////        actions.keyDown(Keys.DELETE).perform();
//        actions.keyDown(ele,Keys.DELETE).perform();
//        Thread.sleep(3000);
//        actions.moveToElement(ele).sendKeys("键盘鼠标操作输入博客内容").perform();
//        Thread.sleep(3000);
        driver.findElement(By.cssSelector("#submit")).click();
        //检查一下博客发布之后是否成功
        String title = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.title")).getText();

        assert title.equals(blogTile);

//        driver.quit();
    }
}
