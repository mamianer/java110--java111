package tests;

import common.Utils;
import org.openqa.selenium.Alert;

import java.io.IOException;

public class PageByNoLogin extends Utils {
    public static String listurl = "http://192.168.47.135:8653/blog_system/blog_list.html";
    public static String editurl = "http://192.168.47.135:8653/blog_system/blog_edit.html";
    public static String detailurl = "http://192.168.47.135:8653/blog_system/blog_detail.html";

    public PageByNoLogin() {
        super("");
    }

    public void ListPageByNoLogin() throws IOException {
        driver.get(listurl);
        //列表页未登录处理

        //处理警告弹窗
        Alert alert = driver.switchTo().alert();
        alert.accept();
        //调整到登录页面
        String expect = driver.getTitle();

        getScreenShot(getClass().getName());

        assert expect.equals("登录页面");
    }

    public void DetailPageByNoLogin() throws IOException {
        //TODO
    }
    public void EditPageByNoLogin() throws IOException {
        //TODO
    }
}
