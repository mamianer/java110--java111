package tests;

import common.Utils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class LoginPage extends Utils {
    public static String url = "http://192.168.47.135:8653/blog_system/blog_login.html";
    public LoginPage() {
        super(url);
    }

    /*
    检查页面是否加载成功
     */
    public void  loginPageRight() throws InterruptedException, IOException {
        //通过查看页面元素是否存在来检查页面加载成功与否
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)"));
        //登陆输入框
        driver.findElement(By.cssSelector("body > div.login-container > form > div"));

    }

    //检查登录功能---成功登录
    public void LoginSuc() throws IOException {
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();

        driver.findElement(By.cssSelector("#username")).sendKeys("admin");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        //检查点击登陆之后是否登陆成功
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a"));
        //页面标题来检查是否登陆成功
        String expect = driver.getTitle();
        assert expect.equals("博客列表");

        getScreenShot(getClass().getName());

        driver.navigate().back();
    }

    /*
    检查登录功能---登录失败
     */
    public void LoginFail() throws IOException {
//        //方法一：通过clear保证输入框没有文本
//        driver.findElement(By.cssSelector("#username")).clear();
//        driver.findElement(By.cssSelector("#password")).clear();

        //方法二：通过刷新保证输入框没有文本
        driver.navigate().refresh();

        driver.findElement(By.cssSelector("#username")).sendKeys("admin111");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();

        //
        String res = driver.findElement(By.cssSelector("body")).getText();


        getScreenShot(getClass().getName());

        assert res.equals("用户名或密码错误!");
    }
}
