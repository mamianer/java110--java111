import tests.EditPage;
import tests.ListPage;
import tests.LoginPage;
import common.Utils;

import java.io.IOException;

public class RunTests {
    public static void main(String[] args) throws InterruptedException, IOException {
        //注意每个用例之间的依赖
        LoginPage login = new LoginPage();
        login.loginPageRight();

        login.LoginSuc();
        login.LoginFail();

        ListPage list = new ListPage();
        list.ListByLogin();

        EditPage edit = new EditPage();
        edit.EditSuc();

        //博客详情页TODO

        //
        Utils.driver.quit();

    }
}
