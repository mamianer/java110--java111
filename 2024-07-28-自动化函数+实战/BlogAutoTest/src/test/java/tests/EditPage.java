package tests;

import common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class EditPage extends Utils {
    public static String url = "http://192.168.47.135:8653/blog_system/blog_edit.html";
    public EditPage() {
        super(url);
    }

    public void EditSuc()
    {

        driver.findElement(By.cssSelector("#title")).sendKeys("Java110&111 Autest01");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
//        wait.until(ExpectedConditions.presenceOfElementLocated());

        //无法输入博客内容---怎么办？
        //两个解决办法：
        //1）博客内容本身就有默认内容，我们不需要手动实现
        //2）还未学到---通过键盘操作来实现
//        WebElement ele = driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre"));
//        ele.sendKeys("比特就业课Java110&111班级");

        //第二种方法实现----下节课再讲

        WebElement ele = driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre"));

        Actions actions = new Actions(driver);
        actions.keyDown(ele, Keys.CONTROL);

        driver.quit();
    }
}
