import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.List;
import java.util.Set;

public class FirstTest {
    WebDriver driver = null;
    void createDriver()
    {
        //1. 打开浏览器 使用驱动来打开
        WebDriverManager.chromedriver().setup();
        //增加浏览器配置：创建驱动对象要强制指定允许访问所有的链接
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");//避免访问链接403

        //设置无头模式
//        options.addArguments("-headless");
        //浏览器加载策略
//        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
//        options.setPageLoadStrategy(PageLoadStrategy.NONE);

        driver = new ChromeDriver(options);
//        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
//        driver.get("https://www.baidu.com");
        //弹窗
//        driver.get("file:///D:/file/%E6%AF%94%E7%89%B9%E6%95%99%E5%8A%A1/%E6%B5%8B%E8%AF%95/selenium4html/selenium-html/alert.html#");
//        driver.get("file:///D:/file/%E6%AF%94%E7%89%B9%E6%95%99%E5%8A%A1/%E6%B5%8B%E8%AF%95/selenium4html/selenium-html/confirm.html");
//        driver.get("file:///D:/file/%E6%AF%94%E7%89%B9%E6%95%99%E5%8A%A1/%E6%B5%8B%E8%AF%95/selenium4html/selenium-html/Prompt.html");
        //文件上传
//        driver.get("file:///D:/file/%E6%AF%94%E7%89%B9%E6%95%99%E5%8A%A1/%E6%B5%8B%E8%AF%95/selenium4html/selenium-html/upload.html");
//        driver.navigate().to("https://tool.lu/");
//        driver.get("file:///D:/dynamic.html");

        driver.get("https://www.bilibili.com/");
    }
    //测试百度搜索关键词；迪丽热巴
    void test01() throws InterruptedException {

        createDriver();
        //2. 输入完整的网址：https://www.baidu.com

        Thread.sleep(3000);

        //3. 找到输入框，并输入关键词：迪丽热巴
        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
        Thread.sleep(3000);

        //4. 找到百度一下按钮，并点击
        driver.findElement(By.cssSelector("#su")).click();
        Thread.sleep(3000);

        //5. 关闭浏览器
        driver.quit();
    }

    //元素的定位
    void test02() throws InterruptedException {
        createDriver();
        //选择器
        driver.findElement(By.cssSelector("#s-hotsearch-wrapper > div"));
        driver.findElement(By.xpath("//*[@id=\"s-hotsearch-wrapper\"]/div"));
        List<WebElement> ll = driver.findElements(By.cssSelector("#hotsearch-content-wrapper > li > a > span.title-content-title"));
        for(int i = 0;i < 6 ;i ++)
        {
            //获取元素Ele对应的文本
            System.out.println(ll.get(i).getText());
        }
        Thread.sleep(8000);
        driver.quit();
    }
    //操作元素
    void  test03() throws InterruptedException {
        createDriver();

//        WebElement ele = driver.findElement(By.cssSelector("#head_wrapper"));
//        ele.click();

        //不可点击元素/隐藏元素
//        driver.findElement(By.cssSelector("#form > input[type=hidden]:nth-child(7)")).click();

//        WebElement ele = driver.findElement(By.cssSelector("#kw"));
//        ele.sendKeys("Java110期");
//        Thread.sleep(2000);
//        ele.clear();
//        Thread.sleep(2000);
//        ele.sendKeys("Java111期");
//        Thread.sleep(2000);

//        WebElement ele = driver.findElement(By.cssSelector("#hotsearch-content-wrapper > li:nth-child(2) > a > span.title-content-title"));
//        //获取元素对应的文本---第二个热搜
//        System.out.println(ele.getText());

        //获取百度一下按钮上的文本
//        String txt = driver.findElement(By.cssSelector("#su")).getText();

//        String txt = driver.findElement(By.cssSelector("#su")).getAttribute("value");
//        System.out.println( "百度一下按钮上的文字为："+txt);

//        String title = driver.getTitle();
//        String url = driver.getCurrentUrl();
//        System.out.println("title:"+title);
//        System.out.println("url:"+url);

        driver.quit();
    }

    //进阶版本的屏幕截图
    void getScreenShot(String str) throws IOException {

        //     ./src/test/image/
        //                     /2024-07-17/
        //                                /test01-17453010.png
        //                                /test02-17453020.png
        //                     /2024-07-18/
        //                                /test01-17453030.png
        //                                /test02-17453034.png
        //屏幕截图
        SimpleDateFormat sim1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sim2 = new SimpleDateFormat("HHmmssSS");

        String dirTime = sim1.format(System.currentTimeMillis());
        String fileTime = sim2.format(System.currentTimeMillis());

        //./src/test/image/2024-07-17/test01-17453020.png
        String filename ="./src/test/image/"+ dirTime +"/" + str + "-" + fileTime+".png";
        System.out.println("filename:"+filename);
        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //srcFile放到指定位置
        FileUtils.copyFile(srcFile,new File(filename));
    }

    void test04() throws InterruptedException, IOException {
        createDriver();
        //设置窗口大小
/*        Thread.sleep(3000);
        driver.manage().window().minimize();

        Thread.sleep(3000);
        driver.manage().window().maximize();

        Thread.sleep(3000);
        driver.manage().window().fullscreen();

        Thread.sleep(3000);
        driver.manage().window().setSize(new Dimension(1624,724));

        Thread.sleep(3000);*/
        getScreenShot(getClass().getName());

        //点击新闻
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();

        String curHandle = driver.getWindowHandle();

        Set<String> allHandles =  driver.getWindowHandles();
        for(String handle : allHandles)
        {
            if(handle != curHandle)
            {
                //切换driver---百度新闻
                driver.switchTo().window(handle);
            }
        }
//        Thread.sleep(3000);

        //测试百度新闻首页

//        //屏幕截图
//        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//        //srcFile放到指定位置
//        FileUtils.copyFile(srcFile,new File("my.png"));

        getScreenShot(getClass().getName());

        driver.findElement(By.cssSelector("#headline-tabs > ul"));

        Thread.sleep(3000);
        driver.close();//关闭当前的标签页

        driver.switchTo().window(curHandle);
        //切换窗口
        Set<String> Handles =  driver.getWindowHandles();
        for(String handle : Handles)
        {
            System.out.println("handle:"+handle);
        }

//        Thread.sleep(3000);
        driver.close();//关闭当前的标签页

        driver.quit();
    }

    void test05() throws InterruptedException {
        createDriver();
//        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
//        driver.findElement(By.cssSelector("#su")).click();
////        Thread.sleep(3000);
//
//
//
////        driver.findElement(By.cssSelector("#\\31  > div > div > div > div > div > div.new-tag_4ozgi.new-text-link_3k9GD > div > div.flex-wrapper-top_3ucFS > div.flex-col-left_3trtY.baike-wrapper_6AORN.cu-pt-xs-lg.baike-wrapper-pc_26R04.cu-pt-xl.baike-wrapper-left-pc_5eYY8.cos-space-pb-sm > div > div > p > span:nth-child(1) > span"));
//        driver.findElement(By.xpath("//*[@id=\"1\"]/div/div/div/div/div/div[4]/div/div[1]/div[1]/div/div/p/span[1]/span"));
//
//        driver.findElement(By.cssSelector("#\\38  > div"));
//
//        driver.findElement(By.cssSelector("#kw")).clear();
//
//        driver.findElement(By.cssSelector("#kw")).sendKeys("邓紫棋");
//        driver.findElement(By.cssSelector("#su")).click();
//        Thread.sleep(3000);
//        WebElement ele = driver.findElement(By.cssSelector("#\\31  > div > div > div > div > div > div.cos-row.row-text_1L24W.row_4WY55 > div > div.title-wrapper_XLSiK > a > div > p > span > span"));
//        System.out.println("txt:"+ele.getText());
        //连续写法
        new WebDriverWait(driver,Duration.ofSeconds(3)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#su")));
         //分开写
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(3));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#su")));

        driver.findElement(By.cssSelector("#su")).click();

        //百度输入框是否存在
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#kw")));
//        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");

        wait.until(ExpectedConditions.textToBe(By.cssSelector("#s-top-left > a:nth-child(1)"),"新闻1"));

        //显示等待
        driver.quit();
    }

//    void test06()
//    {
//        createDriver();
////        driver.findElement(By.cssSelector("#dynamic-zjqe5r"));
//        driver.findElement(By.cssSelector("body > div > section > article"));
//        driver.quit();
//    }
    void test07() throws InterruptedException {
        createDriver();

        Thread.sleep(2000);
        //点击进入到文库
        driver.findElement(By.cssSelector("#nav > div > div > ul > li:nth-child(3) > a")).click();
        Thread.sleep(2000);
        //浏览器后退---工具
        driver.navigate().back();
        Thread.sleep(2000);
        //浏览器前进---文库
        driver.navigate().forward();
        Thread.sleep(2000);

        //浏览器刷新---文库
        driver.navigate().refresh();
        Thread.sleep(2000);

        driver.quit();
    }

    void test08() throws InterruptedException {
        createDriver();
//        调起弹窗
//        driver.findElement(By.cssSelector("#tooltip")).click();
//        Thread.sleep(2000);
//        //警告弹窗
//        //切换弹窗
//        Alert alert = driver.switchTo().alert();
//        //点击确认
//        alert.accept();
//        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("#tooltip"));

//        Thread.sleep(2000);
//        //确认弹窗
//        driver.findElement(By.cssSelector("body > input[type=button]")).click();
//        Thread.sleep(2000);
//        //确认
//        driver.switchTo().alert().accept();
//
//        Thread.sleep(2000);
//        //页面刷新
//        driver.navigate().refresh();
//
//        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("body > input[type=button]")).click();
//        Thread.sleep(2000);
//        //取消
//        driver.switchTo().alert().dismiss();
//        Thread.sleep(2000);

        //提示弹窗
//        driver.findElement(By.cssSelector("body > input[type=button]")).click();
        driver.findElement(By.cssSelector("body > input[type=button11111]")).click();
        Alert alert = driver.switchTo().alert();
        Thread.sleep(2000);
        //输入关键词---从页面是看不到效果的，但实际输入了
        alert.sendKeys("小比特");
        Thread.sleep(2000);
        alert.accept();
        Thread.sleep(2000);

        driver.navigate().refresh();
        driver.findElement(By.cssSelector("body > input[type=button]")).click();
        alert = driver.switchTo().alert();
        alert.dismiss();
        Thread.sleep(2000);
        driver.quit();
    }

    void test09()
    {
        createDriver();
        //隐式等待设置为5s，显?等待设置为10s，那么结果会是5+10=15s吗？
        SimpleDateFormat sim =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sim.format(System.currentTimeMillis()));

        //隐式等待--5秒
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        //强制等待--10s
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        try{
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#hotsearch-content-wrapper > li:nth-child(1) > a > span.title-content")));
        }catch (Exception e){
            System.out.println("nosuelement!");
        }
            System.out.println(sim.format(System.currentTimeMillis()));
        }

        void test10() throws InterruptedException {
            createDriver();
            Thread.sleep(3000);
            WebElement ele = driver.findElement(By.cssSelector("body > div > div > input[type=file]"));
            //把文件路径放到该元素下，相当于文件的上传
            ele.sendKeys("D:\\dynamic.html");

            Thread.sleep(3000);
            driver.quit();
        }

        void test11()
        {
            createDriver();
            driver.quit();
        }
}
