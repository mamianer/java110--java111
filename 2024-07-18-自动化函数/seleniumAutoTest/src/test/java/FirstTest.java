import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

public class FirstTest {
    WebDriver driver = null;
    void createDriver()
    {
        //1. 打开浏览器 使用驱动来打开
        WebDriverManager.chromedriver().setup();
        //增加浏览器配置：创建驱动对象要强制指定允许访问所有的链接
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);
        driver.get("https://www.baidu.com");
    }
    //测试百度搜索关键词；迪丽热巴
    void test01() throws InterruptedException {

        createDriver();
        //2. 输入完整的网址：https://www.baidu.com

        Thread.sleep(3000);

        //3. 找到输入框，并输入关键词：迪丽热巴
        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
        Thread.sleep(3000);

        //4. 找到百度一下按钮，并点击
        driver.findElement(By.cssSelector("#su")).click();
        Thread.sleep(3000);

        //5. 关闭浏览器
        driver.quit();
    }

    //元素的定位
    void test02() throws InterruptedException {
        createDriver();
        //选择器
        driver.findElement(By.cssSelector("#s-hotsearch-wrapper > div"));
        driver.findElement(By.xpath("//*[@id=\"s-hotsearch-wrapper\"]/div"));
        List<WebElement> ll = driver.findElements(By.cssSelector("#hotsearch-content-wrapper > li > a > span.title-content-title"));
        for(int i = 0;i < 6 ;i ++)
        {
            //获取元素Ele对应的文本
            System.out.println(ll.get(i).getText());
        }
        Thread.sleep(8000);
        driver.quit();
    }
    //操作元素
    void  test03() throws InterruptedException {
        createDriver();

//        WebElement ele = driver.findElement(By.cssSelector("#head_wrapper"));
//        ele.click();

        //不可点击元素/隐藏元素
//        driver.findElement(By.cssSelector("#form > input[type=hidden]:nth-child(7)")).click();

//        WebElement ele = driver.findElement(By.cssSelector("#kw"));
//        ele.sendKeys("Java110期");
//        Thread.sleep(2000);
//        ele.clear();
//        Thread.sleep(2000);
//        ele.sendKeys("Java111期");
//        Thread.sleep(2000);

//        WebElement ele = driver.findElement(By.cssSelector("#hotsearch-content-wrapper > li:nth-child(2) > a > span.title-content-title"));
//        //获取元素对应的文本---第二个热搜
//        System.out.println(ele.getText());

        //获取百度一下按钮上的文本
//        String txt = driver.findElement(By.cssSelector("#su")).getText();

//        String txt = driver.findElement(By.cssSelector("#su")).getAttribute("value");
//        System.out.println( "百度一下按钮上的文字为："+txt);

//        String title = driver.getTitle();
//        String url = driver.getCurrentUrl();
//        System.out.println("title:"+title);
//        System.out.println("url:"+url);

        driver.quit();
    }

    //进阶版本的屏幕截图
    void getScreenShot(String str) throws IOException {

        //     ./src/test/image/
        //                     /2024-07-17/
        //                                /test01-174530.png
        //                                /test02-174530.png
        //                     /2024-07-18/
        //                                /test01-174530.png
        //                                /test02-174530.png
        //屏幕截图
        SimpleDateFormat sim1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sim2 = new SimpleDateFormat("HHmmss");

        String dirTime = sim1.format(System.currentTimeMillis());
        String fileTime = sim2.format(System.currentTimeMillis());

        //./src/test/image/2024-07-17/test01-174530.png
        String filename ="./src/test/image/"+ dirTime +"/" + str + "-" + fileTime+".png";
        System.out.println("filename:"+filename);
        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //srcFile放到指定位置
        FileUtils.copyFile(srcFile,new File(fileTime));
    }

    void test04() throws InterruptedException, IOException {
        createDriver();
        //设置窗口大小
/*        Thread.sleep(3000);
        driver.manage().window().minimize();

        Thread.sleep(3000);
        driver.manage().window().maximize();

        Thread.sleep(3000);
        driver.manage().window().fullscreen();

        Thread.sleep(3000);
        driver.manage().window().setSize(new Dimension(1624,724));

        Thread.sleep(3000);*/

        //点击新闻
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();

        String curHandle = driver.getWindowHandle();

        Set<String> allHandles =  driver.getWindowHandles();
        for(String handle : allHandles)
        {
            if(handle != curHandle)
            {
                //切换driver---百度新闻
                driver.switchTo().window(handle);
            }
        }
//        Thread.sleep(3000);

        //测试百度新闻首页

//        //屏幕截图
//        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//        //srcFile放到指定位置
//        FileUtils.copyFile(srcFile,new File("my.png"));

        getScreenShot(getClass().getName());

        driver.findElement(By.cssSelector("#headline-tabs > ul"));
        driver.quit();
    }
}
